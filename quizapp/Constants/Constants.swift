//
//  Textfield.swift
//  quizapp
//
//  Created by Admin on 23/06/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import UIKit

let base_url = "https://quizadmin.steamdaily.com/api"
let login_url = "\(base_url)/auth/login"
let registration_url = "\(base_url)/auth/register"

extension UITextField {
    
    func bottomLine() {
        
        // Creating the bottomLine
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width , height: 2)
        bottomLine.backgroundColor = UIColor.lightGray.cgColor
        
        // Removing the border
        self.borderStyle = .none
        
        // Adding line to the textfield
        self.layer.addSublayer(bottomLine)
        
        self.layer.masksToBounds = true
    }
}

extension CALayer {
    
    func border() {
        
        let topBorder = CALayer()
        let rightBorder:CALayer = CALayer()
        let bottomBorder:CALayer = CALayer()        
        
        topBorder.borderWidth = 1
        topBorder.borderColor = UIColor.lightGray.cgColor
        rightBorder.borderWidth = 1
        rightBorder.borderColor = UIColor.lightGray.cgColor
        bottomBorder.borderWidth = 1
        bottomBorder.borderColor = UIColor.lightGray.cgColor
        topBorder.frame = CGRect(x: 0, y: 0, width: frame.width , height: 1)
        rightBorder.frame = CGRect(x: frame.width - 1, y: 0 , width: 1, height: frame.height)
        bottomBorder.frame = CGRect(x: 0, y: frame.height, width: frame.width , height: 1)
        self.addSublayer(topBorder)
        self.addSublayer(rightBorder)
        self.addSublayer(bottomBorder)
        
    }

    
}
extension UIViewController {
    
    func showAlert(withTitle title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
        })
        _ = UIAlertAction(title: "Cancel", style: .default, handler: { action in
        })
        alert.addAction(ok)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
}

@IBDesignable class button: UIButton {}

extension UIView {
    
   
    @IBInspectable var borderWidth:CGFloat {
        set {
            self.layer.borderWidth = newValue
        }
        get {
           
            return self.layer.borderWidth
        }
    }
    
    @IBInspectable var borderColor:UIColor {
           set {
               self.layer.borderColor = (newValue as UIColor).cgColor
           }
           get {
               let color = self.layer.borderColor
               return UIColor(cgColor: color!)
           }
       }
       
    
    @IBInspectable var cornerRadius:CGFloat {
        set {
            self.layer.cornerRadius = newValue
        }
        get {
           
            return self.layer.cornerRadius
        }
    }
    
    @IBInspectable var masksToBounds:Bool {
        set {
            self.layer.masksToBounds = newValue
        }
        get {

            return self.layer.masksToBounds
        }
    }
    
}
