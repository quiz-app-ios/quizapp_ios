//
//  ActivationStepTwoViewController.swift
//  quizapp
//
//  Created by Admin on 23/06/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class ActivationStepTwoViewController: UIViewController {

    @IBOutlet weak var lbltwo: UILabel!
    @IBOutlet weak var codeOne: UITextField!
    @IBOutlet weak var codeTwo: UITextField!
    @IBOutlet weak var codeThree: UITextField!
    @IBOutlet weak var codeFour: UITextField!
    
    @IBOutlet weak var codeFive: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    
        codeOne.bottomLine()
        codeTwo.bottomLine()
        codeThree.bottomLine()
        codeFour.bottomLine()
        codeFive.bottomLine()
      
    }
    

  
    @IBAction func backTapped(_ sender: Any) {
        
        let activationVc = self.storyboard?.instantiateViewController(identifier: "ActivationStepOneViewController") as! ActivationStepOneViewController
        self.navigationController?.pushViewController(activationVc, animated: true)
    }
    
}
