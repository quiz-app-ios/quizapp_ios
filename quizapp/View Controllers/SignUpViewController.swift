//
//  SignUpViewController.swift
//  quizapp
//
//  Created by Admin on 23/06/20.
//  Copyright © 2020 Admin. All rights reserved.
//

struct registerModel: Encodable {
    let name:String
    let email:String
    let password:String
    let confirmPassword:String
    let studentClass:String
}

import UIKit
import Alamofire
import SwiftyJSON
class SignUpViewController: UIViewController {
    
    @IBOutlet weak var userNme: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var pwd: UITextField!
    @IBOutlet weak var studentClass: UITextField!
    @IBOutlet weak var passwordConfirmation: UITextField!
    @IBOutlet weak var createAccBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userNme.bottomLine()
        email.bottomLine()
        pwd.bottomLine()
        passwordConfirmation.bottomLine()
        studentClass.bottomLine()
        
    }
    // MARK: SignUp API
    @IBAction func createAcctTapped(_ sender: Any) {
        let parameters : [String:String] = [
            "name" : userNme.text!,
            "email":email.text!,
            "password":pwd.text!,
            "password_confirmation":passwordConfirmation.text!,
            "class":studentClass.text!
        ]
        ServerClass.sharedInstance.sendMultipartRequestToServerWithParameter(urlString: registration_url, sendJson: parameters, successBlock: { json in
            let status = json["status"].stringValue
            print(json)
            if status == "ok" {
                self.showAlert(withTitle: "Alert", message: "Success")
                let activationVc = self.storyboard?.instantiateViewController(identifier: "ActivationStepOneViewController") as! ActivationStepOneViewController
                self.navigationController?.pushViewController(activationVc, animated: true)
            }
            else {
                self.showAlert(withTitle: "Alert", message: json["message"].stringValue)
            }
        }, errorBlock: { (NSError) in
            self.showAlert(withTitle: "Alert", message: "Unexpected error")
            
        })
    }
    
    //        guard let name = userNme.text else { return }
    //        guard let email = email.text else { return }
    //        guard let password = pwd.text else { return }
    //        guard let confirmPassword = confirmPassword.text else { return }
    //        guard let studentClass = studentClass.text else { return }
    //
    //        let register = registerModel(name: name, email: email, password: password, confirmPassword: confirmPassword, studentClass: studentClass)
    //        APIManager.sharedInstance.callingRegisterAPI(register: register) { (isSuccess, str) in
    //            if isSuccess {
    //                //self.showAlert(withTitle: "Alert", message: str)
    //                let activationVc = self.storyboard?.instantiateViewController(identifier: "ActivationStepOneViewController") as! ActivationStepOneViewController
    //                self.navigationController?.pushViewController(activationVc, animated: true)
    //            } else {
    //                self.showAlert(withTitle: "Alert", message: str)
    //            }
    //
    //        }
    
    
    
    @IBAction func logInTapped(_ sender: Any) {
        
        let logInVc = self.storyboard?.instantiateViewController(identifier: "LogInViewController") as! LogInViewController
        self.navigationController?.pushViewController(logInVc, animated: true)
    }
}

