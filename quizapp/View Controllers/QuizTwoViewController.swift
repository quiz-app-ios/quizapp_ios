//
//  QuizTwoViewController.swift
//  quizapp
//
//  Created by Admin on 01/07/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class QuizTwoViewController: UIViewController {
  
    @IBOutlet weak var notVisLbl: UILabel!
    @IBOutlet weak var answeredLbl: UILabel!
    @IBOutlet weak var notAnsLbl: UILabel!
    @IBOutlet weak var reviewLbl: UILabel!
    @IBOutlet weak var ansNMarkLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notVisLbl.layer.border()
        answeredLbl.layer.border()
        notAnsLbl.layer.border()
        reviewLbl.layer.border()
        ansNMarkLbl.layer.border()
  
    }
    
}
