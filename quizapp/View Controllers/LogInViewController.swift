//
//  LogInViewController.swift
//  quizapp
//
//  Created by Admin on 23/06/20.
//  Copyright © 2020 Admin. All rights reserved.
//

struct loginModel: Encodable {
    let email:String
    let password:String
}

import UIKit
import Alamofire
class LogInViewController: UIViewController {
    
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userName.bottomLine()
        password.bottomLine()
        
    }
    
    //MARK: Login API
    @IBAction func loginTapped(_ sender: Any) {
        let parameters : [String:String] = [
                                            "email":userName.text!,
                                            "password":password.text!,
                                            ]
        ServerClass.sharedInstance.sendMultipartRequestToServerWithParameter(urlString: login_url, sendJson: parameters, successBlock: { (json) in
            let status = json["status"].stringValue
            if status == "ok" {
                
                let subjectVc = self.storyboard?.instantiateViewController(identifier: "SubjectsViewController") as! SubjectsViewController
                                self.navigationController?.pushViewController(subjectVc, animated: true)
            }
            else {
                self.showAlert(withTitle: "Alert", message: json["message"].stringValue)
            }
        }, errorBlock: { error in
            self.showAlert(withTitle: "Error!", message: error.localizedDescription)
                       
                    })
                
            }
//        guard let email = self.userName.text else { return }
//        guard let password = self.password.text else { return }
//        let login = loginModel(email: email, password: password)
//        APIManager.sharedInstance.callingLoginAPI(login: login) { (result) in
//            switch result {
//            case .success(let json):
//                //self.showAlert(withTitle: "Alert", message: "Logged In Successfully")
//                print(json as AnyObject)
//                let subjectVc = self.storyboard?.instantiateViewController(identifier: "SubjectsViewController") as! SubjectsViewController
//                self.navigationController?.pushViewController(subjectVc, animated: true)
//                
//            case .failure(let error):
//                print(error.localizedDescription)
//                self.showAlert(withTitle: "Alert", message: "Please register first")
//            }
//        }
    
    
    @IBAction func signUpTapped(_ sender: UIButton) {
        let signUpVc = self.storyboard?.instantiateViewController(identifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(signUpVc, animated: true)
    }
    
    
}



