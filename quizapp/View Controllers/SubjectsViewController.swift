//
//  SubjectsViewController.swift
//  quizapp
//
//  Created by Admin on 24/06/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class SubjectsViewController: UIViewController {
    
    @IBOutlet weak var phyView: UIStackView!
    @IBOutlet weak var chemisView: UIStackView!
    @IBOutlet weak var bioView: UIView!
    @IBOutlet weak var mathsView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
    }
    
    func setUpView() {
        phyView.layer.cornerRadius = phyView.frame.height/2
        phyView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        
        chemisView.layer.cornerRadius = chemisView.frame.height/2
        chemisView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        
        bioView.layer.cornerRadius = bioView.frame.height/2
        bioView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        
        mathsView.layer.cornerRadius = mathsView.frame.height/2
        mathsView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        
    }
    
}
