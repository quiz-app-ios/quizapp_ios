//
//  GraphDashboardViewController.swift
//  quizapp
//
//  Created by Admin on 30/06/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class GraphDashboardViewController: UIViewController {
    
    
    var taketestarr = ["Units And Measurement","Kinematics","Laws Of Motion","Impulse And Momemtum","Work,Power And Energy","Gravitation","Oscillation","Waves"]
    
    @IBOutlet weak var phyView: UIView!
    @IBOutlet weak var chemisView: UIView!
    @IBOutlet weak var bioView: UIView!
    @IBOutlet weak var mathsView: UIView!
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    func setUpView() {
        phyView.layer.cornerRadius = phyView.frame.height/2
        phyView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        
        chemisView.layer.cornerRadius = chemisView.frame.height/2
        chemisView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        
        bioView.layer.cornerRadius = bioView.frame.height/2
        bioView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        
        mathsView.layer.cornerRadius = mathsView.frame.height/2
        mathsView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
    }
    
}

extension GraphDashboardViewController:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taketestarr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "TakeTestCell") as! UITableViewCell
        cell.textLabel?.text = taketestarr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.5
    }
    
    
    
}
