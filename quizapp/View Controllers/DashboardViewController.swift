//
//  DashboardViewController.swift
//  quizapp
//
//  Created by Admin on 24/06/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    
    
    var testarr = ["Units And Measurement","Kinematics","Laws Of Motion","Impulse And Momemtum","Work,Power And Energy","Gravitation","Oscillation","Waves"]
    
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
    }
    


}

extension DashboardViewController:UITableViewDelegate,UITableViewDataSource {
    
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return testarr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TestCell") as! UITableViewCell
        cell.textLabel?.text = testarr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.5
    }
    
    
}
