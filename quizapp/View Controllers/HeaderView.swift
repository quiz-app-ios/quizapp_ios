//
//  HeaderViewTableViewCell.swift
//  quizapp
//
//  Created by Admin on 26/06/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class HeaderView: UITableViewCell {

    @IBOutlet weak var headerLbl: UILabel!    
    @IBOutlet weak var headerIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
