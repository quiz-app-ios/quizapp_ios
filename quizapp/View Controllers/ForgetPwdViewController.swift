//
//  ForgetPwdViewController.swift
//  quizapp
//
//  Created by Admin on 23/06/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class ForgetPwdViewController: UIViewController {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var logInBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        emailField.bottomLine()
        
    }
    
}
    
