//
//  QuizViewController.swift
//  quizapp
//
//  Created by Admin on 26/06/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class QuizViewController: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var timer: UILabel!
   
    @IBOutlet weak var notVisLbl: UILabel!
    @IBOutlet weak var answeredLbl: UILabel!
    @IBOutlet weak var notAnsLbl: UILabel!
    @IBOutlet weak var reviewLbl: UILabel!
    @IBOutlet weak var ansNmarkLbl: UILabel!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notVisLbl.layer.border()
        answeredLbl.layer.border()
        notAnsLbl.layer.border()
        reviewLbl.layer.border()
        ansNmarkLbl.layer.border()
        
    }
    
}

extension QuizViewController:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "QuestionsCell") as! UITableViewCell
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.lightGray.cgColor
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
}
