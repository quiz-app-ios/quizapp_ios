//
//  ActivationViewController.swift
//  quizapp
//
//  Created by Admin on 23/06/20.
//  Copyright © 2020 Admin. All rights reserved.
//
import UIKit

class ActivationStepOneViewController: UIViewController {

    @IBOutlet weak var lblOne: UILabel!
    @IBOutlet weak var lblTwo: UILabel!
    @IBOutlet weak var phoneNo: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        phoneNo.bottomLine()
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        
        let activationVc = self.storyboard?.instantiateViewController(identifier: "ActivationStepTwoViewController") as! ActivationStepTwoViewController
        self.navigationController?.pushViewController(activationVc, animated: true)
    }
    
}
